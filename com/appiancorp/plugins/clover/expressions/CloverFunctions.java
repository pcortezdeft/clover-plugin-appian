package com.appiancorp.plugins.clover.expressions;

import com.appiancorp.suiteapi.expression.annotations.Category;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Category("cloverCategory")
public class CloverFunctions {
	@Function
	public String getEmployees(@Parameter String merchantid, @Parameter String apikey) {
		OkHttpClient client = new OkHttpClient();
		try {
			Request request = new Request.Builder().url("https://apisandbox.dev.clover.com/v3/merchants/" 
					+ merchantid
					+ "/employees?access_token=" 
					+ apikey)
					.get()
					.addHeader("accept", "application/json")
					.build();

			Response response = client.newCall(request).execute();
			return response.body().string();
		} catch (Exception e) {
			return "There was an error performing the request";
		}
	}

}